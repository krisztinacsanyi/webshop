A Webler Oktatóstúdiónál jelenleg zajló Webmester tanfolyamom augusztus közepéig tart.

Ez a webshop tanulási célból készül(t), jelenleg is fejlesztés alatt áll, csak korlátozottan reszponzív és a design csak annyira készült el, hogy használható legyen az oldal.

Vizsgamunkaként egy hasonló webshopot kell készítenem, felhasználva a projekt készítésekor megszerzett tudást. Ez már teljesen reszponzív lesz és rendesen megtervezett kinézettel fog elkészülni, viszont funckióit tekintve nagyon hasonló lesz. Ezért feltöltöm ezt a munkát, hogy előrevetítsem, tanfolyamom elvégzésekor mit fogok tudni egyedül megcsinálni.

Projektjeim között több olyan oldal is szerepel, ahol a design kapott hangsúlyt. 
