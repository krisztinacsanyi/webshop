<?php require "header.php"; ?>

<div id="top">
    <img id="logo" src="" alt="">
    <?php  require "menu.php";  ?>
</div>

<div id="left">
    <?php require "kategoria.php"; ?>
</div>

<div id="right">
    <div class="szoveg">
        <h2>Megrendelés összesítése</h2>
        <table width="90%" align="center" cellpadding="8">
            <tr align="center">
                <th>Azonosító</th>
                <th>Terméknév</th>
                <th>Bruttó ár</th>
                <th>Darabszám</th>
                <th>Cikkszám</th>
                <th>Érték</th>
            </tr>

            <?php
                error_reporting(0);
                $vegosszeg=0;

                if(isset($_SESSION["cart"])){
                    foreach($_SESSION["cart"] as $product_id => $db){
                        $con=mysqli_connect(host,user,pwd,dbname);
                        mysqli_query($con,"SET NAMES utf8");

                        $sql="select * from termekek where id='$product_id'";
                        $result=mysqli_query($con,$sql);

                        while($row=mysqli_fetch_array($result)){
                            $id=$row["id"];
                            $termeknev=$row["termeknev"];
                            $bruttoar=$row["ar"];
                            $cikkszam=$row["cikkszam"];
                            $ertek=$bruttoar*$db;

                            echo "
                                <tr align='center'>
                                    <td>".$id."</td>
                                    <td>".$termeknev."</td>
                                    <td>".number_format($bruttoar,0,".",".")." Ft</td>
                                    <td>".$db."</td>
                                    <td>".$cikkszam."</td>
                                    <td>".number_format($ertek,0,".",".")." Ft</td>
                                </tr>
                            
                            ";

                            $vegosszeg+=$ertek;
                        }
                    }
                }
            ?>
            <tr align="right">
                <td colspan="6">
                    <strong>Végösszeg: </strong> <?php echo number_format($vegosszeg,0,".",".");?> Ft
                </td>
            </tr>
        </table>
        
        
    <?php
        $error="";
        $error2="";

        if(isset($_POST["megrendel"])&&(isset($_POST["check"])==1)){
            $nev=$_SESSION["user"];
            $email= $_SESSION["email"];
            $telefon=$_POST["szcim"];
            $szcim=$_POST["szcim"];
            $szcim2=$_POST["szcim2"];
            $szmod=$_POST["szmod"];
            $fizmod=$_POST["fizmod"];

            if(empty($telefon)||empty($szcim)||empty($szcim2)){
                $error="Rendelés leadásához minden mező kitöltése kötelező!";
            }
            else{
                $con=mysqli_connect(host,user,pwd,dbname);

                mysqli_query($con,"SET NAMES utf8");

                $sql="insert into vevok(nev,email,cim,telefon,pw,szcim) values('$nev','$email','$szcim2','$telefon','','$szcim')";

                mysqli_query($con,$sql);

                $utolsoid="select id from vevok order by id desc limit 1";

                $result= mysqli_query($con,$utolsoid);

                $get_vevoid=mysqli_fetch_array($result);

                $kapottvevoid = $get_vevoid[0];

                $sql2="insert into rendelesek(vevoid, szallitas, fizmod, datum, statusz, bosszeg) values('$kapottvevoid','$szmod','$fizmod',now(),'függőben','$vegosszeg')";
                
                mysqli_query($con,$sql2);

                $utolsorendeles="select id from rendelesek order by id desc limit 1";

                $result2=mysqli_query($con, $utolsorendeles);

                $get_rendelesid=mysqli_fetch_array($result2);

                $kapottrendelesid=$get_rendelesid[0];

                foreach($_SESSION["cart"] as $product_id => $db){
                    $sql3="insert into rend_term(rendelesid,termekid,db) values('$kapottrendelesid','$product_id','$db')";

                    mysqli_query($con, $sql3);

                    $sql4 ="update termekek set keszlet=keszlet - '$db' where id='$product_id'";
                    mysqli_query($con, $sql4);
                }

                echo "<h3 id='success'>Rendelésed sikeresen rögzítettük!</h3>";
                unset($_SESSION["cart"]);
            }
        }
        else if(isset($_POST["megrendel"]) && $_POST["check"]==0){
            $telefon=$_POST["szcim"];
            $szcim=$_POST["szcim"];
            $szcim2=$_POST["szcim2"];
            $szmod=$_POST["szmod"];
            $fizmod=$_POST["fizmod"];
            
            $error2="Vásárlási feltételek elfogadása kötelező!";

            if(empty($telefon)||empty($szcim)){
                $error="Rendelés leadásához minden mező kitöltése kötelező!";
            }
        }
    ?>
    <div id="megrendeles">
        <section class="formitems">
        <form action="" method="post">

            <h4 class="error">
                <?php 
                    if(!empty($error)){echo $error;}
                ?>
            </h4>
            <input type="text" name="telefon" id="" placeholder="Telefonszám...">
            <input type="text" name="szcim" id="" placeholder="Szállítási cím (irsz,város,utca,hsz)...">
            <input type="text" name="szcim2" id="" placeholder="Számlázási cím (irsz,város,utca,hsz)...">

            <select name="szmod" id="">
                <option value="gls">GLS futárral</option>
                <option value="posta-utanvet">Postai utánvétellel</option>
                <option value="szemelyes">Személyes átvétellel</option>
            </select>
            <select name="fizmod" id="">
                <option value="obk">Online bankkártya</option>
                <option value="utanvet">Utánvét</option>
                <option value="atutalas">Átutalás</option>
            </select>
            <h4 class="error">
                <?php 
                    if(!empty($error2)){echo $error2;}
                ?>
            </h4>
            <p><a href="tajekoztato.php">Elfogadom a vásárlási feltételeket</a></p>
            <input type="checkbox" name="check">

            <button class="megrendelgomb" type="submit" name="megrendel">Rendelés leadása</button>
        </form>
    </section>
    </div>
    </div>
   
</div>

</body>
</html>