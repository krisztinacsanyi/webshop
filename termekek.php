<?php require "header.php";?>

<div id="top">
    <img id="logo" src="" alt="">
    <?php require "menu.php";?>
</div>

<div id="left">
    
    <?php require "kategoria.php";?>
</div>

<div id="right">
    <div class="sortlinks">
        <a href="?sort=price_asc">Ár szerint növekvő</a>
        <a href="?sort=price_desc">Ár szerint csökkenő</a>
        <a href="?sort=newest">Legújabb elől</a>
        <a href="?sort=best">Legnépszerűbb elől</a>
    </div>
       
    <div id="sort">
    <div class='wrapper'>
        <?php
            $con=mysqli_connect(host,user,pwd,dbname);
            mysqli_query($con,"SET NAMES utf8");

            if(isset($_GET["katid"])){
                $katid=$_GET["katid"];
                $sql="select * from termekek where kategoria='$katid'";
            }
            else if(isset($_GET["sort"])){
                $sort=$_GET["sort"];
                switch($sort){
                    case "price_asc":
                        $sql="select * from termekek order by ar asc";
                        break;
                    case "price_desc":
                        $sql="select * from termekek order by ar desc";
                        break;
                    case "newest":
                        $sql="select * from termekek order by id asc";
                        break;
                    case "best":
                        $sql="select * from termekek inner join rend_term on termekek.id=rend_term.termekid group by termeknev order by sum(db) desc";
                        break;
                }
            }
            else{
                $sql="select * from termekek order by id desc";
            }

            $result=mysqli_query($con,$sql);

            while($row=mysqli_fetch_array($result)){
                $id= $row["id"];
                $termeknev= $row["termeknev"];
                $termekar= $row["ar"];
                $termekkep= $row["kep"];
                $keszlet= $row["keszlet"];

                echo "
                
                    <div class='termekdoboz'>
                        <div class='termekkep'>
                            <a href='termek.php?termekid=".$id."'>
                                <img src='$termekkep'>
                            </a>
                        </div>
                        <div class='termeknev'>
                            ".$termeknev."
                        </div>
                        <div class='keszlet'>
                            Készlet: ".$keszlet."
                        </div>
                        <div class='termekar'>
                            ".number_format($termekar)." Ft
                        </div>
                        <div class='termekkosar'>
                            <a href='kosarmuvelet.php?id=".$id."&action=add'>Kosárba</a>
                        </div>
                    </div>
                
                    
                
                ";
            }
        ?>
        </div>
    </div>
</div>

</body>
</html>