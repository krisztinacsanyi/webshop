<?php
    if($_SESSION["logged"]){
?>


<nav>
    <form action="" method="post">
        <a href="index.php">Főoldal</a>
        <a href="termekek.php">Termékek</a>
        <a href="tajekoztato.php">Vásárlói tájékoztató</a>
        <a href="kapcsolat.php">Kapcsolat</a>
        <a href="kereses.php">Keresés</a>
        <a href="kosar.php">Kosár</a>
        <a href="user_adat.php"><?php echo "<i id='user' class='far fa-user'></i> ".$_SESSION["user"];?></a>
        <button type="submit" name="logout" class="logout">Kijelentkezés</button>
    </form>
    
</nav>
<?php
    }
    else{
        $_SESSION["user"] = "";
    
?>
<nav>
    <form action="" method="post">
        <a href="index.php">Főoldal</a>
        <a href="termekek.php">Termékek</a>
        <a href="tajekoztato.php">Vásárlói tájékoztató</a>
        <a href="kapcsolat.php">Kapcsolat</a>
        <a href="kereses.php">Keresés</a>
        <a href="kosar.php">Kosár</a>
        <a href="login_reg.php">Jelentkezz be!</a>
    </form>
    
</nav>
<?php
    }
    if(isset($_POST["logout"])){
        unset($_SESSION["logged"]);
        $_SESSION["logged"] = false;
        header("Location: index.php");
    }
?>