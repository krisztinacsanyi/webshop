<?php require "header.php"; ?>

<div id="top">
    <img id="logo" src="" alt="">
    <?php  require "menu.php";  ?>
</div>

<div id="left">
    <?php require "kategoria.php"; ?>
</div>

<div id="right">
<div class="szoveg">
        <div class="userrend">
            <h2><?php echo $_SESSION["user"]." rendeléseinek megtekintése! "; ?></h2>
            <table width="95%" align="center" cellpadding="7">
                <tr>
                    <th>Azonosító</th>
                    <th>Terméknév</th>
                    <th>Termékár</th>
                    <th>Darabszám</th>
                    <th>Érték</th>
                    <th>Termékkép</th>
                </tr>
                <?php
                    $nev=$_SESSION["user"];
                    $con=mysqli_connect(host,user,pwd,dbname);
                    mysqli_query($con, "SET NAMES utf8");
                    $sql="select termekid, termeknev, ar, db, kep from vevok inner join rendelesek on vevok.id=rendelesek.vevoid inner join rend_term on rendelesek.id=rend_term.rendelesid inner join termekek on rend_term.termekid=termekek.id where vevok.nev like '$nev'";
                    $result=mysqli_query($con, $sql);
                    while($row=mysqli_fetch_array($result)){
                        $id=$row["termekid"];
                        $termeknev=$row["termeknev"];
                        $termekar=$row["ar"];
                        $db=$row["db"];
                        $ertek=$db*$termekar;
                        $termekkep=$row["kep"];

                        echo "
                            <tr align='center'>
                                <td>".$id."</td>
                                <td>".$termeknev."</td>
                                <td>".number_format($termekar,0,".",".")."</td>
                                <td>".$db."</td>
                                <td>".number_format($ertek,0,".",".")."</td>
                                <td><a href='termek.php?termekid=".$id."'><img src='$termekkep' width='32px height='32px'/></a></td>
                            
                            </tr>
                        
                        ";
                    }


                ?>
            </table>
        </div>
    </div>
   
</div>

</body>
</html>
