<?php
    error_reporting(0);
    session_start();
    $_SESSION["logged"]=false;
    $error="";
    $success="";
    require "connection.php";
    if(isset($_POST["reg"])){
        $email = $_POST["email"];
        $password= $_POST["password"];
        $username = $_POST["username"];
        if(empty($email)||empty($password)||empty($username)){
            $error="Minden mező kitöltése kötelező!";
        }
        else if(strlen($password)<8){
            $error="A jelszónak legalább 8 karakternek kell lennie!";
        }
        else if(!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/',$password)){
            $error="A jelszó nem egyezik a követelmenyékkel!";
        }
        else{
            $con=mysqli_connect(host,user,pwd,dbname);
            mysqli_query($con,"SET NAMES utf8");

            $usersql="select * from adatok where user='$username'";
            $user_exist=mysqli_query($con, $usersql);
            if(mysqli_num_rows($user_exist)>=1){
                $error="Létező felhasználónév";
            }
            else{  
                $password=sha1($password);
                $sql= "INSERT INTO adatok(user,email,pwd) VALUES('$username','$email','$password')";
                mysqli_query($con,$sql);
                $success="Sikeres regisztráció!";
            }
        }
    }
    else if(isset($_POST["log"])){
        $email = $_POST["email"];
        $password= sha1($_POST["password"]);
        $username = $_POST["username"];

        $con2=mysqli_connect(host,user,pwd,dbname);
        mysqli_query($con2,"SET NAMES utf8");
        $sql="select * from adatok";
        $result=mysqli_query($con2,$sql);

        while($row=mysqli_fetch_array($result)){

            $email1=$row["email"];
            $password1=$row["pwd"];
            $username1=$row["user"];

            if($email==$email1 && $password==$password1 && $username==$username1)
            {
                $_SESSION["logged"]=true;
                $_SESSION["user"]=$username;
                $_SESSION["email"]=$email;
                header("Location: index.php");
            }
            else{
                $error="Hibás adatok!";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="Description" content="Enter your description here"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
<link rel="stylesheet" href="assets/css/style.css">
<title>Title</title>
</head>
<body>
    <div class="container">
            <div class="row justify-content center">
                <form action="" method="post" class="form-group bg-light text-dark mx-auto p-5">
                <span class="d-block text-danger">

                <?php
                    if(!empty($error)){echo $error;}                
                ?>

                </span>
                <span class="d-block text-success">

                <?php
                    if(!empty($success)){echo $success;}                
                ?>

                </span>
                <div class="form-group">
                    <label>E-mail cím:</label>
                    <input type="email" class="form-control" name="email">        
                </div>
                <div class="form-group">
                    <label>Felhasználónév:</label>
                    <input type="text" class="form-control" name="username">        
                </div>
                <div class="form-group mb-4">
                    <label>Jelszó:</label>
                    <input type="password" class="form-control" name="password">        
                </div>
                <div class="text-center">
                <button type="submit" class="btn btn-info" name="reg">Regisztráció</button>
                <button type="submit" class="btn btn-info" name="log">Bejelentkezés</button>
                </div>
                    <ul class="mt-4 text-muted p-3">
                        A jelszónak az alábbi elemeket kell tartalmaznia:
                        <li>Kisbetű: a-z</li>
                        <li>Nagybetű: A-Z</li>
                        <li>Szám: 0-9</li>
                        <li>Speciális karakter: @#\-_$%^&+=§!\?</li>
                    </ul>
                
                </form>
            </div>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>
</html>
